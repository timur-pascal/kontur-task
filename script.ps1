# This program is test task
# It's clone repository on gin and build it
# In this time push to nuget repository impossible
# Becouse name project is not available
$conf = (Get-Content .\conf.json) -join "`n" | ConvertFrom-Json
$repo = $conf.repository
$check_time = $conf.CheckTime
$user = $conf.User
# This key need to push project in nuget
$key = $conf.nugetKey
# take name project foulder
$name_dir = ([regex]"[^\\/]*$").Matches($conf.repository)
$name_dir = [string]$name_dir
$name_dir = $name_dir.Replace(" ", "")
$isdir = Test-Path (".\" + $name_dir)

if (-not(Get-Command "git" -ErrorAction SilentlyContinue)) {
    $source = "https://github.com/git-for-windows/git/releases/download/v2.18.0.windows.1/Git-2.18.0-32-bit.exe"
    $destination = ".\git.exe"
    Invoke-WebRequest $source -OutFile $destination
    .\git.exe
}

if (-not(Get-Command "nuget" -ErrorAction SilentlyContinue)) {
    $source = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
    $destination = "C:\Program files\Nuget\nuget.exe."
    Invoke-WebRequest $source -OutFile $destination
    $env:path = $env:path + ";C:\Program files\Nuget\"
}

if ($isdir -ne "True") {
    git clone $repo $name_dir
}

Set-Location (".\" + $name_dir)
$old_hash = $conf.hashCommit
# Take new hash commit
$new_hash = git log -1 --pretty=format:%H
# Take name author commit
$conf.package.autor = git log -1 --pretty=format:%cn
# Return if last commit same old commit
if ($new_hash -eq $old_hash) {
    "Ooops... Nothing to do." | Out-Host
    Set-Location ..\
    return
}
else {
    $conf.hashCommit = $new_hash
    # Take name build-file
    $name_proj = Get-ChildItem *.csproj | Sort-Object CreationTime | Select-Object -Last 1 -Exp name
    $title = $conf.title
    $assembly = Get-Content .\Properties\AssemblyInfo.cs
    $assembly = $assembly -replace '(?<=AssemblyTitle\(").*(?="\))', $title
    $assembly > .\Properties\AssemblyInfo.cs
    # Build project
    dotnet build --configuration Release
    $name_proj = $name_proj.Replace(".csproj", ".exe")
    $conf.output = &(".\bin\Release\" + $name_proj)
    $conf.package.description = $conf.output + "`n" + "Hash commit: " + $conf.hashCommit
    $name_proj = $name_proj.Replace(".exe", ".csproj")
    $id = $conf.package.id
    $des = $conf.package.description
    $author = $conf.package.autor
    nuget spec (".\" + $name_proj)
    nuget pack (".\" + $name_proj) -properties author=$author -properties description=$des -properties id=$id -properties title="SomeTitle" -properties configuration=Release
    # Uncomment this if you change name project
    # Push package in nuget
    $pack = Get-ChildItem *.nupkg | Select-Object  -Exp name
    dotnet nuget push (".\" + $pack) -k $key -s https://nuget.org/
    Set-Location "..\"
    $conf | ConvertTo-Json > .\conf.json
    "The program is end." | Out-Host
}
# Set timer for script
# Attention. It's need a password
$isjob = (Get-ScheduledJob UpdateRepository).Name
if ($isjob -ne "UpdateRepository") {
    $t = New-JobTrigger -Daily -At $check_time
    $cred = Get-Credential $user
    $o = New-ScheduledJobOption -RunElevated
    $dir = Get-Location
    $dir = $dir.Path + "\script.ps1"
    Register-ScheduledJob -Name UpdateRepository -FilePath $dir -Credential $cred -ScheduledJobOption $o -Trigger $t
}
